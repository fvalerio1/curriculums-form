import React from 'react';
import Image from 'next/image';

export default function Header() {
  return(
    <div>
      <div className='flex mt-5'>
        <Image
          src="https://api.toto.com.uy:2556/documentos/fvalerio/images/logo.png"
          alt="Logo"
          width={50}
          height={50}
          className=''
        />
        <div className='flex items-center w-screen justify-center'>
          <h1 className='uppercase font-semibold'>Trabaja con nosotros</h1>
        </div>
      </div>
  </div>

)}
