import React, { useState } from "react";
import axios from "axios";
import FileUploader from "./FileUploader";
import FormData from "form-data";

export default function Form() {
  const [nombre, setNombre] = useState("");
  const [apellido, setApellido] = useState("");
  const [email, setEmail] = useState("");
  const [celular, setCelular] = useState("");
  const [fecha, setFecha] = useState("");
  const [selectedFile, setSelectedFile] = useState(null);


  const submitForm = () => {
    let data = new FormData();
    let cv = document.getElementById('cv');
    data.append("Nombre", "Facundo");
    data.append("Apellido", "Valerio");
    data.append("Celular", "091234234");
    data.append("Email", "fac.valerio@gmail.com");
    data.append("Fecha", "01/01/1990");
    data.append("CV", cv.files[0]);

    // data.append(
    //   "",
    //   selectedFile
    // );

    var config = {
      method: "POST",
      url: "https://localhost:44345/api/Email/Send",
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      data: data,
    };

    axios(config)
      .then(function (response) {
        console.log(JSON.stringify(response.data));
      })
      .catch(function (error) {
        console.log(error);
      });

    // const formData = new FormData();
    // formData.append("nombre", nombre);
    // formData.append("apellido", apellido);
    // formData.append("email", email);
    // formData.append("celular", celular);
    // formData.append("fecha", fecha);
    // formData.append("file", selectedFile);

    // const config = {
    //   method: 'POST',
    //   url: 'https://localhost:44345/api/Email/Send',
    //   // headers: {
    //   //   ...formData.getHeaders()
    //   // },
    //   formData : formData
    // };

    // axios(config)
    // .then(function (response) {
    //   console.log(JSON.stringify(response.data));

    // })
    // .catch(function (error) {
    //   console.log(error);
    // });

    // axios
    //   .post('https://localhost:44345/api/Email/Send', formData)
    //   .then((res) => {
    //     alert("File Upload success");
    //   })
    //   .catch((err) => alert("File Upload Error"));
  };

  return (
    <div>
      <section className="max-w-4xl p-6 mx-auto bg-white rounded-md shadow-md dark:bg-gray-800 mt-10">
        <h2 className="text-lg font-semibold text-gray-700 capitalize dark:text-white mb-14">
          Ingresa tus datos
        </h2>

        {/* <form> */}
        <div className="grid grid-cols-1 gap-6 mt-4 sm:grid-cols-2">
          <div>
            <label
              className="text-gray-700 dark:text-gray-200"
              htmlFor="nombre"
            >
              Nombre
            </label>
            <input
              id="nombre"
              type="text"
              className="block w-full px-4 py-2 mt-2 text-gray-700 bg-white border border-gray-200 rounded-md dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-400 focus:ring-blue-300 focus:ring-opacity-40 dark:focus:border-blue-300 focus:outline-none focus:ring"
              onChange={(e) => setNombre(e.target.value)}
            />
          </div>

          <div>
            <label
              className="text-gray-700 dark:text-gray-200"
              htmlFor="apellido"
            >
              Apellido
            </label>
            <input
              id="apellido"
              type="text"
              className="block w-full px-4 py-2 mt-2 text-gray-700 bg-white border border-gray-200 rounded-md dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-400 focus:ring-blue-300 focus:ring-opacity-40 dark:focus:border-blue-300 focus:outline-none focus:ring"
              onChange={(e) => setApellido(e.target.value)}
            />
          </div>

          <div>
            <label
              className="text-gray-700 dark:text-gray-200"
              htmlFor="celular"
            >
              Celular
            </label>
            <input
              id="celular"
              type="number"
              className="block w-full px-4 py-2 mt-2 text-gray-700 bg-white border border-gray-200 rounded-md dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-400 focus:ring-blue-300 focus:ring-opacity-40 dark:focus:border-blue-300 focus:outline-none focus:ring"
              onChange={(e) => setCelular(e.target.value)}
            />
          </div>

          <div>
            <label className="text-gray-700 dark:text-gray-200" htmlFor="email">
              E-mail
            </label>
            <input
              id="email"
              type="email"
              className="block w-full px-4 py-2 mt-2 text-gray-700 bg-white border border-gray-200 rounded-md dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-400 focus:ring-blue-300 focus:ring-opacity-40 dark:focus:border-blue-300 focus:outline-none focus:ring"
              onChange={(e) => setEmail(e.target.value)}
            />
          </div>

          <div>
            <label className="text-gray-700 dark:text-gray-200" htmlFor="fecha">
              Fecha de nacimiento
            </label>
            <input
              id="fecha"
              type="date"
              className="block w-full px-4 py-2 mt-2 text-gray-700 bg-white border border-gray-200 rounded-md dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-400 focus:ring-blue-300 focus:ring-opacity-40 dark:focus:border-blue-300 focus:outline-none focus:ring"
              onChange={(e) => setFecha(e.target.value)}
            />
          </div>

          <div>
            <label className="text-gray-700 dark:text-gray-200" htmlFor="cv">
              Curriculum
            </label>
            <input
              id="cv"
              type="file"
              className="block w-full px-4 py-2 mt-2 text-gray-700 bg-white border border-gray-200 rounded-md dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-400 focus:ring-blue-300 focus:ring-opacity-40 dark:focus:border-blue-300 focus:outline-none focus:ring"
              // onChange={(e) => setSelectedFile(e.target.files[0])}
              onChange={(e) => setSelectedFile(e.target.files[0])}
            />
          </div>
        </div>

        <div className="flex justify-end mt-6">
          <button
            className="px-6 py-2 leading-5 text-white transition-colors duration-200 transhtmlForm bg-gray-700 rounded-md hover:bg-gray-600 focus:outline-none focus:bg-gray-600"
            onClick={submitForm}
          >
            Enviar
          </button>
        </div>
        {/* </form> */}
      </section>
    </div>
  );
}
